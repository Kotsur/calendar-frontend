import axios from "axios";

export default {
    actions: {
        async getAllEventAction(ctx) {
            await axios.get('http://localhost:8081/events')
                .then(response => {
                    ctx.commit('updateAllEvents', JSON.parse(JSON.stringify(response.data)));
                })
        },
        async getEventAction(ctx, idEvent) {
            await axios.get('http://localhost:8081/events/' + idEvent)
                .then(response => {
                    ctx.commit('updateEvent', JSON.parse(JSON.stringify(response.data)));
                })
        },
        setDateAction(ctx, date) {
            ctx.commit('updateDate', date);
        },
        async updateEventAction(ctx, event) {
            await axios.put('http://localhost:8081/events/' + event.id, event)
                .then(response => {
                    ctx.commit('updateEvent', JSON.parse(JSON.stringify(response.data)));
                    ctx.dispatch("getAllEventAction")
                })
        },
        async deleteEventAction(ctx, idEvent) {
            await axios.delete('http://localhost:8081/events/' + idEvent)
            ctx.dispatch("getAllEventAction")
        },
        emptyEvent(ctx){
            ctx.commit('updateEvent', {});
        },
        async createEventAction(ctx, event) {
            await axios.post('http://localhost:8081/events', event)
                .then(response => {
                    ctx.commit('updateEvent', JSON.parse(JSON.stringify(response.data)));
                    ctx.dispatch("getAllEventAction")
                })
        },

    },
    mutations: {
        updateAllEvents(state, events) {
            state.events = events
        },
        updateEvent(state, event) {
            state.event = event
        },
        updateDate(state, date) {
            state.date = date
        },

    },
    state: {
        events: [],
        event: {},
        date: new Date()
    },
    getters: {
        getAllEvents(state) {
            return state.events
        },
        getEvent(state) {
            state.event.startDate = new Date(  state.event.startDate)
            state.event.endDate = new Date(  state.event.endDate)
            return state.event
        },
        getDate(state) {
            return state.date
        }
    },
}
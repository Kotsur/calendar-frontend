import {createRouter, createWebHistory} from 'vue-router'
import ViewCalendar from "@/views/ViewCalendar.vue";
import ViewAddNewEvent from "@/views/ViewAddNewEvent.vue";
import ViewEvent from "@/views/ViewEvent.vue";


const router = createRouter(
    {
        history: createWebHistory(),
        routes: [
            {path: '/index', component: ViewCalendar, alias: '/'},
            {path: '/events/:idEvent', component: ViewEvent},
            {path: '/events/add', component: ViewAddNewEvent},
            // {path: '/static/:idPage', component: StaticPage},
        ],
    })
export default router;
# frontend-calendar

## Copy project
```
git clone https://gitlab.com/Kotsur/calendar-frontend.git
```

## Project setup
```
npm install
```

### Run
```
npm run serve
```
### Open
```
 http://localhost:8080/ 
```